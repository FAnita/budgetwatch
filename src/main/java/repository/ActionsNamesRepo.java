package repository;

import org.springframework.data.repository.CrudRepository;

import database.ActionsNames;

public interface ActionsNamesRepo extends CrudRepository<ActionsNames, Integer> {

}
