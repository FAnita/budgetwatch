package repository;

import org.springframework.data.repository.CrudRepository;

import database.Elements;

public interface ElementRepo extends CrudRepository<Elements, Integer> {

}
