package repository;

import org.springframework.data.repository.CrudRepository;

import database.Savings;

public interface SavingsRepo extends CrudRepository<Savings, Integer> {

}
