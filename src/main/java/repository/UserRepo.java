package repository;

import org.springframework.data.repository.CrudRepository;

import database.User;

public interface UserRepo extends CrudRepository<User, String> {

	User findByName(String name);

	User findByNameAndPassword(String name, int password);

}
