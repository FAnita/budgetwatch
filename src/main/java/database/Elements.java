package database;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Elements {

	@Id
	@GeneratedValue
	private int id;
	//@Enumerated(EnumType.STRING)
	private String action;
	private int amount;
	private String name;
	private Date date;
	private String userID;
	private int newUserAmount;

	public int getNewUSerAmount() {
		return newUserAmount;
	}

	public void setNewUSerAmount(int newUSerAmount) {
		this.newUserAmount = newUSerAmount;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
