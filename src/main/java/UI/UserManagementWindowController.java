package UI;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class UserManagementWindowController {
	@FXML
	private TextField userNameTextField;
	@FXML
	private PasswordField userPassword;
	@FXML
	private TextField userAmountTextField;
	@FXML
	private CheckBox userIsAdminCheckBox;
	@FXML
	private Button saveButton;
	@FXML
	private Button cancelButton;
	public UserManagementWindowController() {
		// TODO Auto-generated constructor stub
	}

}
