package UI;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import database.Savings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import repository.SavingsRepo;

public class SavingViewWindowController implements Initializable {
	@FXML
	private ListView<Savings> savingNameListView;
	@FXML
	private TextArea savingDataTextArea;
	@FXML
	private Button backButton;

	private SavingsRepo logicSavings = Main.context.getBean(SavingsRepo.class);

	@FXML
	private void handlerActionButtonBack() throws IOException {
		UI.Main.changeScene("WindowDesings/MenuWindow.fxml");
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		ObservableList<Savings> list = FXCollections.observableArrayList();
		for (Savings item : logicSavings.findAll()) {
			if (item.getUserId().equals(UI.Main.loginUser.getName())) {
				list.add(item);
			}
		}
		savingNameListView.getItems().setAll(list);
	}

	@FXML
	private void datasWriteToTextArea() {
		Savings item = savingNameListView.getSelectionModel().getSelectedItem();
		if (item != null) {
			savingDataTextArea.setText("A megtakarítás neve: " + item.getName() + "\nA megtakarítás összege: "
					+ item.getAmount() + "\nA megtakarítás kezdő dátuma: " + item.getStartDate().toString());
		}
	}
}
