package UI;

import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.util.ResourceBundle;

import database.Elements;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import repository.ElementRepo;

public class DailyExpensesWindowController implements Initializable {

	@FXML
	private Button backButton;
	@FXML
	private Button searchButton;
	@FXML
	private TextField searchTextField;
	@FXML
	private TableView<Elements> dataViewTableView;
	@FXML
	private TableColumn<Elements, Date> dateColumn;
	@FXML
	private TableColumn<Elements, Integer> amountColumn;
	@FXML
	private TableColumn<Elements, String> nameColumn;
	@FXML
	private TableColumn<Elements, String> actionColumn;
	@FXML
	private Label amountLabel;
	@FXML
	private Label newAmountLabel;

	private ElementRepo logicElement = Main.context.getBean(ElementRepo.class);

	@FXML
	private void handlerActionButtonBack() throws IOException {
		UI.Main.changeScene("WindowDesings/MenuWindow.fxml");
	}

	@FXML
	private void handlerActionButtonSearch() throws IOException {
		setCellWithDatas();

		ObservableList<Elements> list = FXCollections.observableArrayList();
		Iterable<Elements> datas = logicElement.findAll();

		for (Elements item : datas) {
			coloringTablbeRows();
			if ((item.getAction().toLowerCase().contains(searchTextField.getText().toLowerCase())
					|| (item.getAmount() + "").contains(searchTextField.getText())
					|| item.getDate().toString().contains(searchTextField.getText())
					|| item.getName().toLowerCase().contains(searchTextField.getText().toLowerCase()))
					&& item.getUserID().equals(UI.Main.loginUser.getName())) {
				list.add(item);
			}
		}
		dataViewTableView.setItems(list);

		amountLabel.setText(UI.Main.loginUser.getAmount() + "");
	}
	
	@FXML
	private void handlerActionSearch(KeyEvent event) throws IOException {
		if(event.getCode()==event.getCode().ENTER) {
			handlerActionButtonSearch();
		}
	}

	private void coloringTablbeRows() {
		actionColumn.setCellFactory(column -> {
			return new TableCell<Elements, String>() {
				@Override
				protected void updateItem(String item, boolean empty) {
					super.updateItem(item, empty);

					if (item == null || empty) {
						setText(null);
						setStyle("");
					} else {
						setText(item);

						if (item.equals("KIADAS")) {
							// setTextFill(Color.RED);
							getTableRow().setStyle("-fx-background-color: red; -fx-font-weight: bold;");
						} else if (item.equals("BEVETEL")) {
							getTableRow().setStyle("-fx-background-color: lightgreen; -fx-font-weight: bold;");
						} else {
							getTableRow().setStyle("-fx-font-weight: bold;");
						}
					}
				}
			};
		});
	}

	private void setCellWithDatas() {
		dateColumn.setCellValueFactory(new PropertyValueFactory<Elements, Date>("date"));
		amountColumn.setCellValueFactory(new PropertyValueFactory<Elements, Integer>("amount"));
		nameColumn.setCellValueFactory(new PropertyValueFactory<Elements, String>("name"));
		actionColumn.setCellValueFactory(new PropertyValueFactory<Elements, String>("action"));
		actionColumn.setVisible(true);
	}
	@FXML
	private void handlerActionNewAmountLabel() {
		newAmountLabel.setText(dataViewTableView.getSelectionModel().getSelectedItem().getNewUSerAmount()+"");
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		setCellWithDatas();

		ObservableList<Elements> list = FXCollections.observableArrayList();
		Iterable<Elements> datas = logicElement.findAll();

		for (Elements item : datas) {
			coloringTablbeRows();
			if(UI.Main.loginUser.getName().equals(item.getUserID())){
			list.add(item);
			}
		}
		dataViewTableView.setItems(list);

		amountLabel.setText(UI.Main.loginUser.getAmount() + "");
	}

}
