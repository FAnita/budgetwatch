package UI;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

public class MenuWindowController implements Initializable {
	
	@FXML
	private Button dailyExpensesButton;
	@FXML 
	private Button newExpenseIncomeButton;
	@FXML
	private Button savingsViewButton;
	@FXML
	private Button logOutButton;
	@FXML
	private Button usersManagementButton;
	
	@FXML
	private void handlerButtonActionLogout (ActionEvent event) throws IOException{
		UI.Main.changeScene("WindowDesings/LoginWindow.fxml");
	}
	@FXML
	private void handlerButtonActionDailyExpenses (ActionEvent event) throws IOException{
		UI.Main.changeScene("WindowDesings/DailyExpensesWindow.fxml");
	}
	@FXML
	private void handlerButtonActionNewExpenseIncome (ActionEvent event) throws IOException{
		UI.Main.changeScene("WindowDesings/NewExpenseIncomeWindow.fxml");
	}
	@FXML
	private void handlerButtonActionSavingView (ActionEvent event) throws IOException{
		UI.Main.changeScene("WindowDesings/SavingViewWindow.fxml");
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		if (UI.Main.loginUser.isAdmin()) {
			usersManagementButton.setVisible(true);
		}
	}
}
