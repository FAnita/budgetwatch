package UI;

import java.io.IOException;

import database.User;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import repository.UserRepo;

public class LoginWindowController {

	@FXML
	private Button loginButton;
	@FXML
	private TextField loginNameTextField;
	@FXML
	private PasswordField passwordField;
	@FXML
	private Label warningMessageLabel;

	private UserRepo logicUser = Main.context.getBean(UserRepo.class);

	@FXML
	private void handlerButtonActionLogin(ActionEvent event) throws IOException {
		User user = logicUser.findByNameAndPassword(loginNameTextField.getText(), passwordField.getText().hashCode());
		if (user != null) {
			UI.Main.loginUser = user;
			UI.Main.changeScene("WindowDesings/MenuWindow.fxml");
		} else {
			warningMessageLabel.setVisible(true);
		}
	}
	@FXML
	private void handlerButtonActionLoginKeyPressed(KeyEvent event) throws IOException {
		if(event.getCode()==event.getCode().ENTER) {
			handlerButtonActionLogin(null);
		}
	}
}
