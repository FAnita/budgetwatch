package UI;

import java.io.FileInputStream;
import java.io.IOException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import config.SpringConfigurationDataSource;
import config.SpringConfigurationJpa;
import database.User;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class Main extends Application {

	public static User loginUser;

	private static Stage stage;

	public static ApplicationContext context;

	public static void main(String[] args) {
		context = new AnnotationConfigApplicationContext(SpringConfigurationDataSource.class,
				SpringConfigurationJpa.class);

		// adat hozz�ad�sa
		User obj = new User();
		obj.setName("Anya");
		int name = "Anya".hashCode();
		obj.setPassword(name);
		obj.setAmount(50000);
		obj.setAdmin(false);

		//configJpa(obj);
		launch(args);
	}

	public static void configJpa(Object obj) {
		EntityManagerFactory emf = context.getBean(EntityManagerFactory.class);
		EntityManager em = emf.createEntityManager();
		EntityTransaction etr = em.getTransaction();
		etr.begin();

		em.persist(obj);

		etr.commit();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		stage = primaryStage;

		FXMLLoader loader = new FXMLLoader();
		String fxmlDocPath = "WindowDesings/LoginWindow.fxml";
		FileInputStream fxmlStream = new FileInputStream(fxmlDocPath);
		GridPane root = (GridPane) loader.load(fxmlStream);

		Scene scene = new Scene(root);

		stage.setScene(scene);
		stage.setTitle("BudgetWatcher");
		stage.show();
	}

	public static void changeScene(String path) throws IOException {
		stage.close();
		FXMLLoader loader = new FXMLLoader();
		FileInputStream fxmlStream = new FileInputStream(path);
		GridPane root = (GridPane) loader.load(fxmlStream);
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.setTitle("BudgetWatcher");
		stage.show();
	}

}