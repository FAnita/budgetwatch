package UI;

import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.util.ResourceBundle;

import database.ActionsNames;
import database.Elements;
import database.Savings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import repository.ActionsNamesRepo;
import repository.ElementRepo;
import repository.SavingsRepo;
import repository.UserRepo;

enum Actions {
	KIADAS, BEVETEL, MEGTAKARITAS;
}

public class NewExpenseIncomeWindowController implements Initializable {

	@FXML
	private ChoiceBox<Actions> chooseActionChoiceBox;
	@FXML
	private ComboBox<String> nameActionComboBox;
	@FXML
	private DatePicker dateActionDatePicker;
	@FXML
	private TextField amountTextField;
	@FXML
	private Button saveButton;
	@FXML
	private Button cancelButton;

	private UserRepo logicUser = Main.context.getBean(UserRepo.class);
	// private ElementRepo logicElement = Main.context.getBean(ElementRepo.class);
	private ActionsNamesRepo logicActionsNames = Main.context.getBean(ActionsNamesRepo.class);
	private SavingsRepo logicSavings = Main.context.getBean(SavingsRepo.class);

	@FXML
	private void handlerActionButtonSave() throws IOException {
		Elements obj = new Elements();

		try {
			obj.setAction(chooseActionChoiceBox.getValue().toString());
			obj.setDate(Date.valueOf(dateActionDatePicker.getValue()));
			obj.setUserID(UI.Main.loginUser.getName());
			obj.setName(nameActionComboBox.getValue());
			obj.setNewUSerAmount(UI.Main.loginUser.getAmount() + obj.getAmount());

			if (chooseActionChoiceBox.getValue().toString().equals("BEVETEL")) {
				obj.setAmount(Integer.parseInt(amountTextField.getText()));
			} else {
				obj.setAmount(Integer.parseInt(amountTextField.getText()) * -1);
			}
		boolean isActionName = false;
		for (ActionsNames item : logicActionsNames.findAll()) {
			if (item.getName().equals(nameActionComboBox.getValue())) {
				isActionName = true;
				break;
			}
		}
		if (!isActionName) {
			ActionsNames obj2 = new ActionsNames();
			obj2.setName(nameActionComboBox.getValue());
			obj2.setAction(chooseActionChoiceBox.getValue().toString());
			UI.Main.configJpa(obj2);
		}

		if (Actions.MEGTAKARITAS.equals(chooseActionChoiceBox.getValue())) {
			boolean isSaving = false;
			for (Savings item : logicSavings.findAll()) {
				if (item.getName().equals(nameActionComboBox.getValue())) {
					isSaving = true;
					item.setAmount(item.getAmount() - obj.getAmount());
					logicSavings.save(item);
					break;
				}
			}
			if (!isActionName) {
				Savings obj3 = new Savings();
				obj3.setAmount(obj3.getAmount() - obj.getAmount());
				obj3.setName(nameActionComboBox.getValue());
				obj3.setStartDate(Date.valueOf(dateActionDatePicker.getValue()));
				obj3.setUserId(UI.Main.loginUser.getName());
				UI.Main.configJpa(obj3);
			} else {

			}
		}
		UI.Main.configJpa(obj);

		UI.Main.loginUser.setAmount(UI.Main.loginUser.getAmount() + obj.getAmount());
		logicUser.save(UI.Main.loginUser);

		UI.Main.changeScene("WindowDesings/MenuWindow.fxml");
		} catch (NullPointerException e) {
			//Warning message
		}
	}

	@FXML
	private void handlerActionButtonCancel() throws IOException {
		UI.Main.changeScene("WindowDesings/MenuWindow.fxml");
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		chooseActionChoiceBox.getItems().setAll(Actions.values());
		Iterable<ActionsNames> datas = logicActionsNames.findAll();
		if (datas != null) {
			ObservableList<String> list = FXCollections.observableArrayList();
			for (ActionsNames item : datas) {
				list.add(item.getName());
			}
			nameActionComboBox.getItems().setAll(list);
		}
	}
}
