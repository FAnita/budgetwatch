package config;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration
public class SpringConfigurationDataSource {
	
	private static String dbUrl = "jdbc:postgresql://localhost:5432/budgetWatch";
	private static String username = "postgres";
	private static String password = "AlmaFa123";
	
	@Bean
	public DataSource dataSource() {
		return new DriverManagerDataSource(dbUrl, username, password);
	}
}
